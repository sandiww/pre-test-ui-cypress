describe("Submit visa form", () => {
  it("Process check fill visa form", () => {
    cy.visit("https://www.phptravels.net/visa");

    cy.get("#from_country").select("Indonesia", {
      force: true,
    });

    cy.get("#to_country").select("Singapore", {
      force: true,
    });

    cy.get('input[name="checkin"]');

    cy.get("#visa-submit").submit();

    cy.get(".breadcrumb-content > .section-heading > .text-white").should(
      "contain.text",
      "30-05-2022"
    );
  });

  it("validationMessage", () => {
    cy.visit("https://www.phptravels.net/visa");
    cy.get("#visa-submit").within(() => {
      cy.get("#from_country")
        .invoke("prop", "validationMessage")
        .should("equal", "Please select an item in the list.");

      cy.get("#to_country")
        .invoke("prop", "validationMessage")
        .should("equal", "Please select an item in the list.");
    });
    // cy.get("#visa-submit").submit();
  });
});
