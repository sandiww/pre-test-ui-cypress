describe("Submit visa form", () => {
  it("Process check fill visa form", () => {
    cy.visit("https://www.phptravels.net/visa");

    cy.get("#from_country").select("Indonesia", {
      force: true,
    });

    cy.get("#to_country").select("Singapore", {
      force: true,
    });

    cy.get('input[name="checkin"]');

    cy.get("#visa-submit").submit();

    cy.get(".breadcrumb-content > .section-heading > .text-white").should(
      "contain.text",
      "30-05-2022"
    );
  });

  it("Process check fill profile form ", () => {
    cy.get("form").within(() => {
      cy.get('input[name="first_name"]')
        .type("John", { force: true })
        .should("have.value", "John");
      cy.get('input[name="last_name"]')
        .type("Lennon", { force: true })
        .should("have.value", "Lennon");
      cy.get('input[name="email"]')
        .type("john_lennon@gmail.com", { force: true })
        .should("have.value", "john_lennon@gmail.com");
      cy.get('input[name="phone"]')
        .type("081232425262", { force: true })
        .should("have.value", "081232425262");
      cy.get('input[name="date"]')
        .invoke("val")
        .then((text) => {
          expect("30-05-2022").to.equal(text);
        });
      cy.get('textarea[name="notes"]')
        .type("Lorem Ipsum Ammets", { force: true })
        .should("have.value", "Lorem Ipsum Ammets");

      cy.get("#submit").click({ force: true }).should("contain", "Submit");
    });
  });
});
